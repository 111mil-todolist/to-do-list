package com.mycompany.todolist;

import com.mycompany.ui.VistaPrincipal;
import java.sql.SQLException;

public class Main {
    public static void main(String[]args) throws SQLException, ClassNotFoundException{
        VistaPrincipal vista = new VistaPrincipal();
        vista.setVisible(true);
    }
}
