package ClasesDTO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Notas")

public class NotaDTO implements Serializable {

    @Id
    private int id;

    @Column(name = "Título")
    private String titulo;

    @Column(name = "Estado")
    private boolean estado;

    public NotaDTO(int id, String titulo, boolean estado) {
        this.id = id;
        this.titulo = titulo;
        this.estado = estado;
    }
    
    public NotaDTO(String titulo){
        this.titulo = titulo;
    }
    
    @Override
    public String toString(){
        String fila;
        if(this.estado == true){
            fila = "ID: " + id + "   " + this.titulo + "   Estado:Terminado";
        }else{
            fila = "ID: " + id + "   " + this.titulo + "   Estado:Por Terminar";
        }
        return fila;
    }

    public NotaDTO() {
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the estado
     */
    public boolean getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(boolean estado) {
        this.estado = estado;
    }

}
