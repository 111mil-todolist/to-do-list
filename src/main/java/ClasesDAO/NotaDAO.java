package ClasesDAO;

import ClasesDTO.NotaDTO;
import Utils.Conector;
import java.sql.Connection;
import java.sql.SQLException;

public class NotaDAO {

    public void crearNota(NotaDTO nota) {
        try {
            Conector conectar = new Conector();
            Connection conectado = conectar.obtener();
            conectar.stm = conectado.createStatement();
            conectar.stm.executeUpdate("insert into todolist (ID_nota,nota,estado) Values (null," + nota.getTitulo() + ", 0);");
            conectar.cerrar();
        } catch (SQLException ex) {
            System.out.println("Excepción de SQL: " + ex.toString());
        } catch (ClassNotFoundException ex) {
            System.out.println("Excepción de Clase No Encontrada: " + ex.toString());
        }
    }

    public void eliminarNota(int ID_nota) {
        try {
            Conector conectar = new Conector();
            Connection conectado = conectar.obtener();
            conectar.stm = conectado.createStatement();
            conectar.stm.executeUpdate("DELETE * FROM todolist WHERE ID_nota = " + ID_nota + ";");
            conectar.cerrar();
        } catch (SQLException ex) {
            System.out.println("Excepción de SQL: " + ex.toString());
        } catch (ClassNotFoundException ex) {
            System.out.println("Excepción de Clase No Encontrada: " + ex.toString());
        }
    }

    public NotaDTO editarNota(int ID_nota) {
        return null;
    }

    public void mostrarNotas() {
    }
}
